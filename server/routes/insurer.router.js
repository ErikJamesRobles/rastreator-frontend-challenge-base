const Koa = require('Koa')
const app = new Koa()
const KoaBody = require('koa-body')
const KoaRouter = require('koa-router')
const json = require('koa-json')

const fs = require('fs')
const logger = require('../inc/logger')

const router = new KoaRouter({
  prefix: '/insurer'
})

class InsurerRouter {
  static async getAll(ctx) {
    logger.info('Get all insurer')

    ctx.status = 200
    ctx.type = 'application/json'
    ctx.body = await JSON.parse(fs.readFileSync('./server/data/insurer.json', 'utf8'))
  }
}

router.get('/', InsurerRouter.getAll)
module.exports = router
