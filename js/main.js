const insuranceUrl = 'http://localhost:3000/insurances';
const loanUrl = 'http://localhost:3000/loans';

	const insuranceContainer = document.querySelector('.insurance');
	const loanContainer = document.querySelector('.loan');

	let Euro = '&euro;/mes';
	let euro_symbol = '&euro;';

	fetch(insuranceUrl) 
	.then(response => response.json())
	.then(data => {
	let div = document.createElement("div"); 
	let ins_data = div.cloneNode(true);
	let ins_data2 = div.cloneNode(true);
	let ins_data3 = div.cloneNode(true);
	let ins_data4 = div.cloneNode(true);
	let ins_data5 = div.cloneNode(true);
	let ins_data6 = div.cloneNode(true);
	let ins_data7 = div.cloneNode(true);
	let ins_data8 = div.cloneNode(true);
	let ins_data9 = div.cloneNode(true);
	
	parseInt(data['mapfre'].basic.price_from);
	parseInt(data['linea-directa'].plus.price_from);
	parseInt(data['mutua'].full.price_from);
	
	let basics1 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd;  font-weight:600; line-height: 20px;">'+ data['linea-directa'].basic.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['linea-directa'].basic.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data.innerHTML = basics1;

	let basics2 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mutua'].basic.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mutua'].basic.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data2.innerHTML = basics2;

	let basics3 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mapfre'].basic.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mapfre'].basic.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data3.innerHTML = basics3;

	let plus1 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px;  text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mutua'].plus.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mutua'].plus.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data4.innerHTML = plus1;

	let plus2 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['linea-directa'].plus.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['linea-directa'].plus.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data5.innerHTML = plus2;

	let plus3 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mutua'].plus.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mutua'].plus.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data6.innerHTML = plus3;

	let full1 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mapfre'].full.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mapfre'].full.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data7.innerHTML = full1;

	let full2 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['mutua'].full.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['mutua'].full.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data8.innerHTML = full2;

	let full3 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
				  +'<label style="color:#27accd; font-weight:600; line-height: 20px;">'+ data['linea-directa'].full.title + '</label>'
				  +'<br />'
				  +'<label style="color:gray; line-height: 20px;">desde</label>'
				  +'<br />'
				  +'<label style="font-weight: bold; line-height: 20px;">'+ data['linea-directa'].full.price_from + ' ' + Euro +  '</label>'
				  +'</div>';
	ins_data9.innerHTML = full3;

	insuranceContainer.appendChild(ins_data);
	insuranceContainer.appendChild(ins_data2);
	insuranceContainer.appendChild(ins_data3);
	insuranceContainer.appendChild(ins_data4);
	insuranceContainer.appendChild(ins_data5);
	insuranceContainer.appendChild(ins_data6);
	insuranceContainer.appendChild(ins_data7);
	insuranceContainer.appendChild(ins_data8);
	insuranceContainer.appendChild(ins_data9);
	});
	fetch(loanUrl)
		.then(response => response.json())
		.then(data => {
			let div = document.createElement("div"); 
			let loan_data1 = div.cloneNode(true);
			let loan_data2 = div.cloneNode(true);
			let loan_data3 = div.cloneNode(true);
			let loan_data4 = div.cloneNode(true);
			let loan_data5 = div.cloneNode(true);
			let loan_data6 = div.cloneNode(true);
			let loan_data7 = div.cloneNode(true);

        let loan1 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['cofidis']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['cofidis']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data1.innerHTML= loan1;
		
		let loan2 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['bbva']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['bbva']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data2.innerHTML= loan2;

		let loan3 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['bankia']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['bankia']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data3.innerHTML= loan3;
		
		let loan4 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['ing']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['ing']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data4.innerHTML= loan4;

		let loan5 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['bankinter']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['bankinter']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data5.innerHTML= loan5;
		

		let loan6 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['popular']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['popular']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data6.innerHTML= loan6;
		
		let loan7 = '<div style="border:1px solid #c4c4c4; margin-right: 10px; padding: 15px; text-align: left;">'
					+'<label style="color:#27accd; font-weight:600; line-height: 20px;">'
					+'Hasta '
					+data['evo']['max']+' '+euro_symbol+'</label>'
                    +'<br />'
                    +'<label style="color:gray; line-height: 20px;">Cuota Desde</label>'
                    +'<br />'
                    +'<label style="font-weight: bold; line-height: 20px;">'+data['evo']['fee']+' '+Euro+'</label>'
					+'</div>';
		loan_data7.innerHTML= loan7;
			loanContainer.appendChild(loan_data1);
			loanContainer.appendChild(loan_data2);
			loanContainer.appendChild(loan_data3);
			loanContainer.appendChild(loan_data4);
			loanContainer.appendChild(loan_data5);
			loanContainer.appendChild(loan_data6);
			loanContainer.appendChild(loan_data7);
		});

