<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu();
		$context['site'] = $this;
		return $context;
	}

	

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

// Scripts
function rct_wp_enqueue_scripts() {
	wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.4.1.min.js', array('jquery'), '3.4.1');
	wp_enqueue_script('jquery');

	wp_register_script('bxsliderjs', get_template_directory_uri() . '/js/jquery.bxslider.js', array('jquery'), '4.2.1'); // BxSlider
	wp_enqueue_script('bxsliderjs');
}

add_action( 'wp_enqueue_scripts', 'rct_wp_enqueue_scripts' );

//Styles
function rct_wp_enqueue_styles() {
	wp_register_style('bxslidercss', get_template_directory_uri() . '/assets/css/jquery.bxslider.css', array(), '1.0', 'all');
	wp_enqueue_style('bxslidercss');
}

add_action( 'wp_enqueue_scripts', 'rct_wp_enqueue_styles' );

/*------------------------------------*/
//External Module filesize
/*------------------------------------*/


new StarterSite();
